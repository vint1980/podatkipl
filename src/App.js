import React, { Component } from 'react';
import SettingsForm from './SettingsForm';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      monthSalaryPkup: 10000,
      monthSalary: 404,
      pkupPercentage: 80,
      withSpouse: false,
      incomeByMonth: [],
      detailedZUS: false,
      showB2B: false,
    };
    for (let month=1; month <= 12; month += 1) {
      this.state.incomeByMonth.push({
        wPkup: this.state.monthSalaryPkup,
        woPkup: this.state.monthSalary,
      });
    }
    this.state.maxPkup = 85000;
    this.pkupBaseCumulative = 0;
    this.taxBaseCumulative = 0;

    this.handleMonthIncome = this.handleMonthIncome.bind(this);
    this.handlePropertyChange = this.onPropertyChange.bind(this);

    this.zusRates = {
      pension: 0.0976,
      rental: 0.015,
      hospital: 0.0245,
      medical: 0.09,
    };
    this.zusRates.total = (({pension, rental, hospital, medical}) => {
      return pension + rental + hospital + medical;
    })(this.zusRates);
  }

  onPropertyChange(property, value) {
    let state = {
      [property]: value,
    };
    if (property === 'monthSalaryPkup') {
      state['incomeByMonth'] = this.state.incomeByMonth.map((income) => {
        return {
          wPkup: value,
          woPkup: income.woPkup,
        }
      });
    }
    if (property === 'monthSalary') {
      state['incomeByMonth'] = this.state.incomeByMonth.map((income) => {
        return {
          wPkup: income.wPkup,
          woPkup: value,
        }
      });
    }
    this.setState(state);
  }

  handleMonthIncome(event) {
    const target = event.target;
    const property = target.name;
    const month = target.parentNode.parentNode.dataset.month;
    const value = Number.isNaN(parseFloat(event.target.value)) ? 0 : parseFloat(event.target.value);
    if (!property || !month || !value) return;
    let {incomeByMonth} = this.state;
    incomeByMonth[month*1][property] = value;
    this.setState({ incomeByMonth });
  }

  readyToMathTaxes() {
    return this.state.monthSalary > 0 && this.state.pkupPercentage >= 0
  }

  yearPayments() {
    const yearData = [];
    this.pkupBaseCumulative = 0;
    this.taxBaseCumulative = 0;
    if (!this.readyToMathTaxes()) return yearData;
    for (let month=1; month<=12; month++) {
      yearData.push(this.mathMonthPayments(month));
    }
    return yearData;
  }

  paymentsTotal(yearData) {
    return yearData.reduce((acc, monthData) => {
      return {
        income: {
          wPkup: acc.income.wPkup + monthData.income.wPkup,
          woPkup: acc.income.woPkup + monthData.income.woPkup,
          total: acc.income.total + monthData.income.total,
        },
        zus: {
          pension: acc.zus.pension + monthData.zus.pension,
          rental: acc.zus.rental + monthData.zus.rental,
          hospital: acc.zus.hospital + monthData.zus.hospital,
          medical: acc.zus.medical + monthData.zus.medical,
          total: acc.zus.total + monthData.zus.total,
        },
        taxes: {
          pkupBase: acc.taxes.pkupBase + monthData.taxes.pkupBase,
          taxFreeBase: acc.taxes.taxFreeBase + monthData.taxes.taxFreeBase,
          fullTaxBase: acc.taxes.fullTaxBase + monthData.taxes.fullTaxBase,
          fullTax: acc.taxes.fullTax + monthData.taxes.fullTax,
        },
        netto: acc.netto + monthData.netto,
        b2b: {
          zus: acc.b2b.zus + monthData.b2b.zus,
          taxBase: acc.b2b.taxBase + monthData.b2b.taxBase,
          tax: acc.b2b.tax + monthData.b2b.tax,
          netto: acc.b2b.netto + monthData.b2b.netto,
        },
      };
    }, {
      income: {
        wPkup: 0,
        woPkup: 0,
        total: 0,
      },
      zus: {
        pension: 0,
        rental: 0,
        hospital: 0,
        medical: 0,
        total: 0,
      },
      taxes: {
        pkupBase: 0,
        taxFreeBase: 0,
        fullTaxBase: 0,
        fullTax: 0,
      },
      netto: 0,
      b2b: {
        zus: 0,
        taxBase: 0,
        tax: 0,
        netto: 0,
      },
    });
  }

  mathMonthPayments(month) {
    const rates = this.zusRates;
    const b2bZus = 1200;
    const b2bTaxRate = 0.18;
    const monthSalary = this.state.incomeByMonth[month-1].woPkup;
    const monthSalaryPkup = this.state.incomeByMonth[month-1].wPkup;
    const brutto = monthSalary + monthSalaryPkup;
    const payments = {
      income: {
        wPkup: monthSalaryPkup,
        woPkup: monthSalary,
        total: brutto,
      },
      zus: {
        pension: rates.pension * brutto,
        rental: rates.rental * brutto,
        hospital: rates.hospital * brutto,
        medical: rates.medical * brutto,
      },
      b2b: {
        zus: b2bZus,
        taxBase: brutto - b2bZus,
        tax: (brutto - b2bZus) * b2bTaxRate,
        netto: brutto - b2bZus - (brutto - b2bZus) * b2bTaxRate,
      },
    };
    const zusRate = rates.pension + rates.rental + rates.hospital + rates.medical;

    payments.zus.total = payments.zus.pension + 1*payments.zus.rental +
                             payments.zus.hospital + 1*payments.zus.medical;
    const bruttoMinusZus = brutto - payments.zus.total;
    const monthSalaryPkupMinusZus = monthSalaryPkup * (1 - zusRate);

    payments.taxes = {};
    payments.taxes.pkupBase = this.getPkupBase(monthSalaryPkupMinusZus * this.state.pkupPercentage / 100);
    payments.taxes.taxFreeBase = payments.taxes.pkupBase / 2;

    payments.taxes.fullTaxBase = monthSalaryPkupMinusZus - payments.taxes.taxFreeBase + monthSalary * (1 - zusRate);
    [payments.taxes.fullTax, payments.taxes.rate] = this.calcTax(payments.taxes.fullTaxBase);

    payments.netto = bruttoMinusZus - payments.taxes.fullTax;
    return payments;
  }

  calcTax(taxBase) {
    const taxRate = 0.18;
    const upTaxRate = 0.32;
    const upTaxLevel = this.state.withSpouse ? 85000 * 2 : 85000;
    const diff = upTaxLevel - this.taxBaseCumulative;
    let tax, rate;
    if (diff > taxBase) {
      tax = taxBase * taxRate;
      rate = 0;
    } else if (diff > 0) {
      tax = (taxBase - diff) * upTaxRate + diff * taxRate;
      rate = 1;
    } else {
      tax = taxBase * upTaxRate;
      rate = 2;
    }
    this.taxBaseCumulative += taxBase;
    return [tax, rate];
  }

  getPkupBase(addition=0) {
    const maxPkup = this.state.maxPkup;
    if (this.pkupBaseCumulative === maxPkup) {
      return 0;
    }
    this.pkupBaseCumulative += addition;
    const overload = this.pkupBaseCumulative - maxPkup;
    if (overload > 0) {
      this.pkupBaseCumulative = maxPkup;
      return addition - overload;
    }
    return addition;
  }

  render() {
    const yearPayments = this.yearPayments();
    const total = this.paymentsTotal(yearPayments);
    const displayOptions = {
      detailedZUS: this.state.detailedZUS,
      showB2B: this.state.showB2B,
    };

    const monthes = yearPayments.map((monthPaymentsData, row) => {
      return (
        <MonthPayments
          options={displayOptions}
          data={monthPaymentsData}
          income={this.state.incomeByMonth[row]}
          monthNum={row}
          key={row}
          onChange={this.handleMonthIncome}
        />
      );
    });


    return (
      <div className="App">
        <h2>Tax calculator for poland workers.</h2>
        <SettingsForm onChange={this.handlePropertyChange} data={this.state} />

        <Notes zusRates={this.zusRates} maxPkup={this.state.maxPkup}/>
        <table border="1" cellPadding="5" cellSpacing="0">
          <DataHead options={displayOptions} />

          <tbody>
            {monthes}
            <YearTotal options={displayOptions} data={total} />
          </tbody>
        </table>
      </div>
    );
  }
}

function DataHead(props) {
  const options = props.options;
  const zusHead = 'pension rental hospital medical total'.split(' ').map((key) => {
    const hide = key !== 'total' && !options.detailedZUS;
    return (<HeadCell value={key} hide={hide} key={key} />);
  });
  const b2bHead = options.showB2B ? (<th colSpan="4">B2B</th>) : null;
  const b2bSubHead = options.showB2B ? (
      <React.Fragment>
        <th>ZUS</th>
        <th>Tax Base</th>
        <th>Tax (18%)</th>
        <th>Netto</th>
      </React.Fragment>
  ) : null;
  return (
    <thead>
      <tr>
        <th rowSpan="2">Month</th>
        <th colSpan="3">Income</th>
        <th colSpan={options.detailedZUS ? 5 : 0}>ZUS payments</th>
        <th colSpan="4">Taxes</th>
        <th rowSpan="2">Netto</th>
        {b2bHead}
      </tr>
      <tr>
        <th>w/PKUP</th>
        <th>wo/PKUP</th>
        <th>total</th>

        {zusHead}

        <th>PKUP base</th>
        <th>Tax Free base</th>
        <th>Tax base</th>
        <th>Tax</th>

        {b2bSubHead}
      </tr>
    </thead>
  );
}


function zusPayments(data, detailed=false) {
  return 'pension rental hospital medical total'.split(' ').map((key) => {
    const hide = key !== 'total' && !detailed;
    return (<DataCell value={data[key]} hide={hide} key={key} className={key === 'total' ? 'total' : ''} />);
  });
}

function YearTotal(props) {
  const data = props.data;

  const b2bPayments = props.options.showB2B ? (
    <React.Fragment>
      <td>{data.b2b.zus}</td>
      <td>{data.b2b.taxBase}</td>
      <td>{data.b2b.tax.toFixed(1)}</td>
      <td className="netto">{data.b2b.netto.toFixed(1)}</td>
    </React.Fragment>
  ) : null;

  return (
    <tr className="total">
      <td>Total</td>

      <td>{data.income.wPkup}</td>
      <td>{data.income.woPkup}</td>
      <td>{data.income.total}</td>

      {zusPayments(data.zus, props.options.detailedZUS)}

      <td>{data.taxes.pkupBase.toFixed(1)}</td>
      <td>{data.taxes.taxFreeBase.toFixed(1)}</td>
      <td>{data.taxes.fullTaxBase.toFixed(1)}</td>
      <td>{data.taxes.fullTax.toFixed(1)}</td>
      <td className="netto">{data.netto.toFixed(1)}</td>

      {b2bPayments}
    </tr>
  );
}

function MonthPayments(props) {
  const data = props.data;
  const warnClass = props.data.taxes.rate ? (props.data.taxes.rate === 1 ? 'warn' : 'crit') : '';

  const b2bPayments = props.options.showB2B ? (
    <React.Fragment>
      <td>{data.b2b.zus}</td>
      <td>{data.b2b.taxBase}</td>
      <td>{data.b2b.tax.toFixed(1)}</td>
      <td className="total">{data.b2b.netto.toFixed(1)}</td>
    </React.Fragment>
  ) : null;

  return (
    <tr className={props.className} data-month={props.monthNum}>
      <td>{props.monthNum+1}</td>

      <td><input type="text" name="wPkup" value={props.income.wPkup} onChange={props.onChange} size="6"/></td>
      <td><input type="text" name="woPkup" value={props.income.woPkup} onChange={props.onChange} size="6"/></td>
      <th>{data.income.total}</th>

      {zusPayments(data.zus, props.options.detailedZUS)}

      <td>{data.taxes.pkupBase.toFixed(1)}</td>
      <td>{data.taxes.taxFreeBase.toFixed(1)}</td>
      <td className={warnClass}>{data.taxes.fullTaxBase.toFixed(1)}</td>
      <td>{data.taxes.fullTax.toFixed(1)}</td>
      <td className="total">{data.netto.toFixed(1)}</td>

      {b2bPayments}

    </tr>
  );
}

function HeadCell(props) {
  if (props.hide) return null;
  return (
    <th>{props.value}</th>
  );
}

function DataCell(props) {
  const {hide, value, ...elementOptions} = props;
  if (hide) return null;
  return (
    <td {...elementOptions}>{value.toFixed(1)}</td>
  );
}


function Notes(props) {
  const zusRates = props.zusRates;
  const maxPkup = props.maxPkup;

  return (
    <div className="notes">
      <p>Social, medical and pension payments from brutto:
        social (pension — {zusRates.pension*100}%, rental — {zusRates.rental*100} %, hospital — {zusRates.hospital*100} %) and medical — {zusRates.medical*100} %.
      </p>
      <p>PKUP base is your %-age of your PKUP available salary minus {zusRates.total*100}% of ZUS payments.
         Bonuses are not PKUP available. Half of PKUP base is Tax free base.
      </p>
      <p>Max PKUP base can not exceed {maxPkup}zł yearly.</p>
      <p>Tax base is your salary minus ZUS payments and minus half of PKUP base.</p>
      <p>For annual tax Base till 85000zł you pay 18% tax, for the exceeding part of annual base tax is 32%.</p>
      <p>Netto is what you will get on hands.</p>
    </div>
  );
}

export default App;
