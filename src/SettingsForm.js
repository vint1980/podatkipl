import React, { Component } from 'react';

class SettingsForm extends Component {
  constructor(props) {
    super(props);
    this.onInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const value = this.getTargetValue(event.target);
    const property = event.target.name;
    if (!property) return;
    this.props.onChange(property, value);
  }

  getTargetValue(target) {
    if (target.type === 'checkbox') {
      return target.checked;
    }
    return Number.isNaN(parseFloat(target.value)) ? 0 : parseFloat(target.value);
  }

  render() {
    const data = this.props.data;
    return (
      <form>
        <div className="row">
          <label>PKUP available month salary:</label>
          <input name="monthSalaryPkup" value={data.monthSalaryPkup} onChange={this.onInputChange} />
        </div>
        <div className="row">
          <label>Non PKUP month salary (bonuses):</label>
          <input name="monthSalary" value={data.monthSalary} onChange={this.onInputChange} />
        </div>
        <div className="row">
          <label>I have PKUP at level of:</label>
          <input name="pkupPercentage" value={data.pkupPercentage} onChange={this.onInputChange} />
          <span>%</span>
        </div>
        <div className="row">
          <label>I submit taxes with my spouse:</label>
          <input name="withSpouse" type="checkbox" checked={data.withSpouse} onChange={this.onInputChange} />
        </div>
        <div className="row">
          <label>Show detailed ZUS payments:</label>
          <input name="detailedZUS" type="checkbox" checked={data.detailedZUS} onChange={this.onInputChange} />
        </div>
        <div className="row">
          <label>Show payments for B2B:</label>
          <input name="showB2B" type="checkbox" checked={data.showB2B} onChange={this.onInputChange} />
        </div>

      </form>
    );
  }
}

export default SettingsForm;